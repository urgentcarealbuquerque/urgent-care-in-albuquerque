**Albuquerque urgent care**

At Urgent Care Albuquerque, you will get the care you need while your primary care provider is unavailable to see you. 
Albuquerque provides hospital treatment for minor illnesses and injuries such as asthma, pneumonia, ear or 
sinus infections, fever, rashes, facial cuts, minor broken bones, and joint sprains. 
Urgent Care Albuquerque provides regular visits at your convenience on the same day.
Do not postpone treatment for medical or urgent needs. 
Our Emergency Treatment Albuquerque is available for infections and injuries that can't wait. For in-person visits, we follow all directions from the CDC.
Please Visit Our Website [Albuquerque urgent care](https://urgentcarealbuquerque.com) for more information. 

---

## Our urgent care in Albuquerque  services

During COVID-19, we suspended all self-scheduling options for emergency care. 
By contacting the clinic of your convenience, you can still make an appointment with Albuquerque Urgent Care. 
When we've got rooms, walk-in appointments are accepted. 
For your ease, we recommend that you schedule a visit to validate your appointment.
If you want to get in, please contact Albuquerque Urgent Care first to clarify the current waiting period and if the availability is available. 
Visiting walk-in patients is recommended as early as possible throughout the day for at least one hour before closing. 
Please be mindful that appointments could be delayed in order to satisfy patients with more pressing needs.
Patients under the age of 18 must have a parent or legal guardian present unless they are seen as a condition protected by the law.

